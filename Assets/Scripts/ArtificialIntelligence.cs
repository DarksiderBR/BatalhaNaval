﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ArtificialIntelligence {

	private CellData[,] enemyCells;
	private int[,] chance = new int[10,10];

	private Ship enemyDestroyer = new Ship("Destroyer", 2);
	private Ship enemySubmarine = new Ship("Submarine", 3);
	private Ship enemyCruiser = new Ship("Cruiser", 3);
	private Ship enemyBattleship = new Ship("Battleship", 4);
	private Ship enemyAircraftCarrier = new Ship("AircraftCarrier", 5);
    

    Node positionDecisionTreeRoot;

    public ArtificialIntelligence()
    {
        positionDecisionTreeRoot = Node.getInstance();
    }

    public void positionShips(BoardData board)
    {

        bool isFirstTimePlaying = !positionDecisionTreeRoot.hasChildren();


        if (!isFirstTimePlaying)
        {

            while (board.hasShipToInstantiate())
            {
                int shipSize = board.getShipSize();
                System.Random rnd = new System.Random();
                int x, y = 0;

                bool shipDirection;
                
                if (rnd.Next(0, 2) == 1)
                {
                    shipDirection = true;
                    x = rnd.Next(0, shipSize + 1);
                    y = rnd.Next(0, 10);

                }
                else
                {
                    shipDirection = false;
                    x = rnd.Next(0, 10);
                    y = rnd.Next(0, shipSize + 1);
                }

                GameObject ship = board.CreateShip(board.cells[x, y], shipDirection);
                if (ship != null)
                {
                    board.setSelectedShip(ship);
                    board.ConfirmShipPosition(board.cells[x, y], shipDirection);
                }
            }
        }
        else
        {

            while (board.hasShipToInstantiate())
            {
                int shipSize = board.getShipSize();
                List<CellData> listToPosition = new List<CellData>();
                listToPosition = evaluatePosition(board, 0, 0, true, shipSize, listToPosition, 9999999);
                System.Random rnd = new System.Random();
                int position = rnd.Next(0, listToPosition.Count - 1);


                GameObject ship = board.CreateShip(board.cells[listToPosition[position].x, listToPosition[position].y], listToPosition[position].bestPositionDirection);
                if (ship != null)
                {
                    board.setSelectedShip(ship);
                    board.ConfirmShipPosition(board.cells[listToPosition[position].x, listToPosition[position].y], listToPosition[position].bestPositionDirection);
                }
            }
        }     
    }

    private List<CellData> evaluatePosition(BoardData board, int x, int y, bool direction, int shipSize, List<CellData> bestPossibleList, int lesserValue)
    {

        // rodando por x até ultima 
        if (x + shipSize > 10 && y < 9 && direction == true)
        {
            x = 0;
            y++;
        }
        else if (x + shipSize > 10 && y == 9 && direction == true)
        {
            x = 0;
            y = 0;
            direction = false;
        }
        else if (y + shipSize >10 && x <9 && direction == false)
        {
            x++;
            y = 0;
        }
        else if (y + shipSize > 10 && x == 9 && direction == false)
        {
            return bestPossibleList;
        }

        //pegar posicao inicial do board
        CellData position = board.cells[x, y];

        //se posicao incial é valida 
        if (board.checkPosition(position, direction, shipSize))
        {
            //Debug.Log(x + " " + y);
            int clicksPerPositionIndex = 0;
            int xyPosition = 0;
            if (direction){
                xyPosition = x;
                for (int i = 0; i < shipSize; i++)
                {
                    clicksPerPositionIndex += board.cells[xyPosition, y].getTimesClicked();
                    xyPosition++;
                }
           }
            else
            {
                xyPosition = y;
                for (int i = 0; i < shipSize; i++)
                {
                    clicksPerPositionIndex += board.cells[x, xyPosition].getTimesClicked();
                    xyPosition++;
                }
            }
                       
            if(clicksPerPositionIndex == lesserValue)
            {
                bestPossibleList.Add(position);
                position.bestPositionDirection = direction;
            }
            else if (clicksPerPositionIndex < lesserValue)
            {
                lesserValue = clicksPerPositionIndex;
                //Debug.Log("valor" + lesserValue);
                bestPossibleList.Clear();
                position.bestPositionDirection = direction;
                bestPossibleList.Add(position);
            }

        }
        if (direction)
        {
            x++;
        }
        else
        {
            y++;
        }

       return evaluatePosition(board, x , y , direction, shipSize, bestPossibleList, lesserValue);
        
    }

	public void Shoot()
	{
		enemyCells = GameController.instance.playerBoard.cells;
		int highestChance = 0;
		CellData highestChanceCell = null;

		for (int x = 0; x < 10; x++) {
			for (int y = 0; y < 10; y++) {
				chance [x, y] = 0;
			}
		}

		for(int x = 0; x < 10; x++)
		{
			for (int y = 0; y < 10; y++)
			{
				CalculateChance (x, y, 0, 0, 1);
			}
		}

		List<CellData> highests = new List<CellData> ();
		System.Random rnd = new System.Random();

		for (int x = 0; x < 10; x++) 
		{
//			Debug.Log (chance [0, x] + " " + chance [1, x] + " " + chance [2, x] + " "
//				+ chance [3, x] + " " + chance [4, x] + " " + chance [5, x] + " "
//				+ chance [6, x] + " " + chance [7, x] + " " + chance [8, x] + " "
//				+ chance [9, x]);
			for (int y = 0; y < 10; y++) 
			{
				if (chance [x, y] > highestChance) {
					highestChance = chance [x, y];
					highests.Clear ();
					highests.Add (enemyCells [x, y]);
				}
				else if (chance [x, y] == highestChance) 
				{
					if (rnd.Next (0, 2) == 0){
						highestChance = chance [x, y];
						highests.Add (enemyCells [x, y]);
					}
				}
			}
		}
		highestChanceCell = highests[rnd.Next (0, highests.Count)];
		if (highestChanceCell != null)
			highestChanceCell.CellAttack ();

		if (highestChanceCell.getValue () != null) 
		{
			string shipName = highestChanceCell.getValue ().transform.parent.name;
			Debug.Log (shipName);
			switch (shipName) 
			{
			case "Destroyer":
				enemyDestroyer.PartFound (highestChanceCell);
				break;
			case "Submarine":
				enemySubmarine.PartFound (highestChanceCell);
				break;
			case "Cruiser":
				enemyCruiser.PartFound (highestChanceCell);
				break;
			case "Battleship":
				enemyBattleship.PartFound (highestChanceCell);
				break;
			case "AircraftCarrier":
				enemyAircraftCarrier.PartFound (highestChanceCell);
				break;
			}

            if (GameController.instance.playerBoard.getNumberOfKilledShips() == 5) {
                GameController.instance.setGameFinished(GameController.instance.AIBoard);
            }
        }
	}

	private int CalculateChance(int x, int y, int iterations, int direction, int value)
	{
		if (x < 0 || x > 9 || y < 0 || y > 9 || !enemyCells[x,y].affectChance || iterations == 5)
			return 0;

		if (!enemyCells [x, y].cellEnabled)
			value += 100;

		int cellChance = 0;

		switch (direction) 
		{
		case 0: 
			cellChance += CalculateChance (x+1, y, iterations+1, 1, value); //right
			cellChance += CalculateChance (x, y+1, iterations+1, 2, value); //up
			cellChance += CalculateChance (x-1, y, iterations+1, 3, value); //left
			cellChance += CalculateChance (x, y-1, iterations+1, 4, value); //down
			break;
		case 1:	
			cellChance += CalculateChance (x+1, y, iterations+1, 1, value); //right
			break;
		case 2: 
			cellChance += CalculateChance (x, y+1, iterations+1, 2, value); //up
			break;
		case 3: 
			cellChance += CalculateChance (x-1, y, iterations+1, 3, value); //left
			break;
		case 4: 
			cellChance += CalculateChance (x, y-1, iterations+1, 4, value); //down
			break;
		}

		switch (iterations) {
		case 1:
			if(enemyDestroyer.isAlive)
				cellChance += value;
			break;
		case 2:
			if(enemyCruiser.isAlive)
				cellChance += value;
			if(enemySubmarine.isAlive)
				cellChance += value;
			break;
		case 3:
			if(enemyBattleship.isAlive)
				cellChance += value;
			break;
		case 4:
			if(enemyAircraftCarrier.isAlive)
				cellChance += value;
			break;
		}
		if(enemyCells[x,y].cellEnabled)
			chance [x, y] += cellChance;

		return cellChance;
	}
}
