﻿using UnityEngine;

public class BoardData : MonoBehaviour {
	public CellData[,] cells = new CellData[10,10];

    private GameObject[] ships;

	private GameObject selectedShip;
	private CellData lastCell;
	private bool direction;
    private int numberOfShipsKilled;
    public int AIMissingCells = 0;

    public void setSelectedShip(GameObject ship)
    {
        selectedShip = ship;
    }

    public int getNumberOfKilledShips()
    {
        return numberOfShipsKilled;
    }


    public void ChangeDirection()
	{
		direction = !direction;
		if (selectedShip != null)
			RepositionShip (lastCell, direction);
	}

    public bool checkPosition(CellData firstCell, bool shipDirection, int size)
    {
        if (shipDirection)
        {
            if ((firstCell.x + size) > 10)
            {
                //Debug.Log("Tamanho do Navio ultrapassa numero de casas do board");
                return false;
            }
                        
            int xPosition = firstCell.x;
            for(int x = 0; x < size; x++)
            {
                
                if(cells[xPosition,firstCell.y].getValue() != null)
                {
                    //Debug.Log("Já tem um navio em uma das casas");
                    return false;
                }
                xPosition++;
            }

        }
        else
        {
			if ((firstCell.y + size) > 10)
			{
				//Debug.Log("Tamanho do Navio ultrapassa numero de casas do board");
				return false;
			}

			int yPosition = firstCell.y;

			for(int y = 0; y < size; y++)
			{

				if(cells[firstCell.x,yPosition].getValue() != null)
				{
					//Debug.Log("Já tem um navio em uma das casas");
					return false;
				}
				yPosition++;
			}
        }
        return true;

    }


    public GameObject CreateShip(CellData clickedCell, bool shipDirection)
    {
        int size = getShipSize();

        if (!checkPosition(clickedCell, shipDirection, size))
        {
            return null;
		}
        

        GameObject shipPrefab = getPrefabShip();
        CellData adjacentCellData = clickedCell;

		GameObject ship = new GameObject ();
		ship.name = shipPrefab.name;
		ship.transform.position = clickedCell.transform.position;

        //horizontal
        if (shipDirection)
        {
            
            for(int x = 0; x < size; x++)
            { 
				createShipPart (shipPrefab, cells [adjacentCellData.x+x, adjacentCellData.y], ship);
            }
        }
        else
        {
			for(int y = 0; y < size; y++)
			{
				createShipPart(shipPrefab, cells[adjacentCellData.x,adjacentCellData.y+y], ship);
			}
        }
		return ship;
    }

	private GameObject createShipPart(GameObject shipPrefab, CellData clickedCell, GameObject parent)
    {   
        GameObject part = (GameObject) Instantiate(shipPrefab);

		if (this.gameObject.name == "Enemy Board") {
			part.GetComponent<SpriteRenderer> ().enabled = false;
		}

        part.transform.SetParent(parent.transform);
        part.transform.position = clickedCell.transform.position;
        part.transform.rotation = clickedCell.transform.rotation;
        part.transform.localScale = this.transform.localScale;

		return part;
    }

	private bool RepositionShip(CellData initialCell, bool shipDirection)
	{
		//Transform[] partsPosition = selectedShip.GetComponentsInChildren<GameObject> ();
		int size = getShipSize ();
		if (selectedShip == null)
		{
			selectedShip = CreateShip (cells [initialCell.x, initialCell.y], direction);
		}
		if (checkPosition (initialCell, shipDirection, size)) 
		{
			Transform[] partsPosition = selectedShip.GetComponentsInChildren<Transform> ();
			selectedShip.transform.position = initialCell.transform.position;
			if (shipDirection)
			{
				for(int x = 0; x < size; x++)
				{ 
					partsPosition[x+1].position = cells[initialCell.x + x, initialCell.y].transform.position;
				}
			}
			else
			{
				for(int y = 0; y < size; y++)
				{
					partsPosition[y+1].position = cells[initialCell.x, initialCell.y + y].transform.position;
				}
			}
			return true;
		}
		return false;
	}

	public bool ConfirmShipPosition(CellData clickedCell, bool shipDirection)
	{
		int size = getShipSize ();
		if (RepositionShip(clickedCell, shipDirection))
		{
			Transform[] parts = selectedShip.GetComponentsInChildren<Transform> ();
			if (shipDirection)
			{
				for(int x = 0; x < size; x++)
				{ 
					cells [clickedCell.x + x, clickedCell.y].setValue (parts[x+1].gameObject);
					//Debug.Log ("Parte colocada na célula "  + cells [clickedCell.x + x, clickedCell.y]);
				}
			}
			else
			{
				for(int y = 0; y < size; y++)
				{ 
					cells [clickedCell.x, clickedCell.y + y].setValue (parts[y+1].gameObject);
					//Debug.Log ("Parte colocada na célula "  + cells [clickedCell.x, clickedCell.y + y]);
				}
			}
			selectedShip = null;
			for (int i = 0; i < ships.Length; i++) 
			{
				if (ships [i] != null) 
				{
					ships [i] = null;
					return true;
				}
			}
		}
		return false;
	}

    public void CellClicked(int x, int y){
		//Debug.Log (cells [x, y]);

		if (this.gameObject.name == "Player Board") {
			if (hasShipToInstantiate ()) {
				if (selectedShip == null) {
					selectedShip = CreateShip (cells [x, y], direction);
				}
				ConfirmShipPosition (cells [x, y], direction);

				if (hasShipToInstantiate () == false) {
					GameController.instance.gameStarted = true;
				}
			}
		} 
		else 
		{
			if (GameController.instance.gameStarted && GameController.instance.isPlayerTurn) 
			{
				if(cells [x, y].CellAttack ())
					cells[x,y].incrementTimesClicked();
                if ( AIMissingCells == 17)
                {
                    GameController.instance.setGameFinished(GameController.instance.playerBoard);
                }
			}
		}
    }

	public void CellEntered(int x, int y){
		if (this.gameObject.name == "Player Board")
		{
			if (hasShipToInstantiate())
			{
				lastCell = cells [x, y];
				if (selectedShip == null) 
				{
					selectedShip = CreateShip (lastCell, direction);
				}
				else 
				{
					RepositionShip (lastCell, direction);
				}
			}
		}
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}



   public void initializeShips(GameObject aircraftCarrier, GameObject battleship, GameObject submarine, GameObject cruiser, GameObject destroyer)
    {
        ships = new GameObject[5];
        ships[0] = aircraftCarrier;
        ships[1] = battleship;
        ships[2] = submarine;
        ships[3] = cruiser;
        ships[4] = destroyer;
    }

    public int getShipSize()
    {
        if (ships[0] != null)
            return 5;
        else if (ships[1] != null)
            return 4;
        else if (ships[2] != null)
            return 3;
        else if (ships[3] != null)
            return 3;
        else
            return 2;
    }

    private GameObject getPrefabShip()
    {

        for (int x = 0; x < ships.Length; x++)
        {
            if (ships[x] != null)
            {
				return ships[x];
            }
        }
        return null;
    }

    public bool hasShipToInstantiate()
    {
        for (int x = 0; x < ships.Length; x++)
        {
            if (ships[x] != null)
            {
                return true;
            }
        }
        return false;
    }

    public void addNumberOfShipsKilled()
    {
        numberOfShipsKilled++;
    }

    
}
