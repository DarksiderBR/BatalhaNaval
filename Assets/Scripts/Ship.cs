﻿using UnityEngine;
using System.Collections;

public class Ship {
	public string name;
	public int size;
	public CellData[] parts;
	public bool isAlive = true;

	public Ship(string name, int size){
		this.name = name;
		this.size = size;
		parts = new CellData[size];
	}

	public void PartFound(CellData cell){
		for (int i = 0; i < parts.Length; i++) {
			if (parts [i] == null) {
				parts [i] = cell;
				break;
			}
		}
		if (parts [parts.Length - 1] != null) {
			Debug.Log (name + "destruido");
			isAlive = false;
            GameController.instance.playerBoard.addNumberOfShipsKilled();
            for (int i = 0; i < parts.Length; i++) {
				parts [i].affectChance = false;
			}
		}
	}
}
