﻿using System.Collections.Generic;

public class Node {
    private static Node instance;
    private int value;
    private int x;
    private int y;
    private Node parent;
    private List<Node> children;

    public static Node getInstance()
    {
        if (instance == null)
        {
            instance = new Node();
        }
        return instance;
    }

    private Node()
    {
        children = new List<Node>();
    }

    public void addChild(Node child)
    {
        children.Add(child);
    }

    public bool hasChildren()
    {
       return (children.Count == 0 ? false : true) ;
    }



}
