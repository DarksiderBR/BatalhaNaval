﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class CellData : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler {
	public int x, y;
	public BoardData board;
    public GameObject value;
    private int timesClicked;
	public bool cellEnabled = true;
	public bool affectChance = true;
    public bool bestPositionDirection;

    public int getTimesClicked()
    {
        return PlayerPrefs.GetInt("IA Cell " + x + "-" + y + " Clicks");
    }

	public void incrementTimesClicked()
	{
		timesClicked++;
		PlayerPrefs.SetInt ("IA Cell " + x + "-" + y + " Clicks", timesClicked);
		//Debug.Log (PlayerPrefs.GetInt("IA Cell " + x + "-" + y + " Clicks"));
	}

    public void setValue(GameObject value)
    {
        this.value = value;
    }

    public GameObject getValue()
    {
        return this.value;
    }


	public void OnPointerClick(PointerEventData e){
		board.CellClicked (x, y);
	}

	public void OnPointerEnter(PointerEventData e){
		board.CellEntered (x, y);
	}

	public bool CellAttack()
	{
		if (cellEnabled == false) 
		{
			return false;
		}
		if (value != null)
		{
			SpriteRenderer shipRenderer = value.GetComponent<SpriteRenderer> ();
			shipRenderer.enabled = true;
			Color c = shipRenderer.color;
			c.a = 0.5f;
			shipRenderer.color = c;
			this.GetComponent<SpriteRenderer> ().color = Color.black;

            board.AIMissingCells++;
            Debug.Log((GameController.instance.isPlayerTurn ? "Você" : "Inteligencia Artificial") + " Acertou");

        } 
		else 
		{
            Debug.Log((GameController.instance.isPlayerTurn ? "Você" : "Inteligencia Artificial") + " Errou");
			affectChance = false;
			SpriteRenderer cellRenderer = this.GetComponent<SpriteRenderer> ();
			cellRenderer.color = Color.gray;
		}
		cellEnabled = false;
		GameController.instance.isPlayerTurn = !GameController.instance.isPlayerTurn;
		return true;
	}

	// Use this for initialization
	void Start () {
		if(board.name == "Enemy Board")
			timesClicked = PlayerPrefs.GetInt ("IA Cell " + x + "-" + y + " Clicks", 0);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
