﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class GameController : MonoBehaviour {
	public static GameController instance = null;

	public BoardData playerBoard;
	public BoardData AIBoard;

	[SerializeField] private GameObject cellPrefab;
	[SerializeField] private GameObject aircraftCarrier;
	[SerializeField] private GameObject battleship;
	[SerializeField] private GameObject submarine;
	[SerializeField] private GameObject cruiser;
	[SerializeField] private GameObject destroyer;

	public bool isPlayerTurn;
	public bool gameStarted = false;
    public bool gameFinished = false;
    public string winnerName;

	public ArtificialIntelligence AI;

    public bool isGameFinished()
    {
        return gameFinished;
    }

    public void setGameFinished(BoardData board)
    {
        gameFinished = true;
        winnerName = board.name;
    }

    // Use this for initialization
    void Start () {
		if (instance == null) 
		{
			instance = this;
		} else 
		{
			Destroy (this.gameObject);
		}

		float aspectScale = Camera.main.aspect * 0.35f;
		playerBoard = CreateBoard (10, Camera.main.ScreenToWorldPoint (new Vector2(Screen.width * 0.05f, Screen.height * 0.6f)), new Vector2(aspectScale, aspectScale), "Player Board");
		AIBoard = CreateBoard (10, Camera.main.ScreenToWorldPoint (new Vector2(Screen.width * 0.5f, Screen.height * 0.4f)), new Vector2(aspectScale, aspectScale), "Enemy Board");

        AI = new ArtificialIntelligence();
        AI.positionShips(AIBoard);

		if (Random.Range (0, 2) == 0) {
			isPlayerTurn = true;
		} else 
		{
			isPlayerTurn = false;
		}
    }

    // Update is called once per frame
    void Update () {

        if (!isGameFinished())
        {

            if (Input.GetKeyDown (KeyCode.Space)) {
			playerBoard.ChangeDirection ();
		    }
            if (gameStarted && !isPlayerTurn)
            {
                AI.Shoot();
                
            }
	    }

        if (isGameFinished())
        {
            Debug.Log(winnerName + " é o ganhador");
            Application.LoadLevel(0);

        }
    }

    private BoardData CreateBoard (int size, Vector2 initialPosition, Vector2 scale, string boardName) {
		GameObject board = new GameObject (boardName);
		GameObject cellsParent = new GameObject ("Cells");
		BoardData boardData = board.AddComponent<BoardData> ();
        boardData.initializeShips(aircraftCarrier, battleship, submarine, cruiser, destroyer);
		board.transform.position = initialPosition;
		cellsParent.transform.SetParent (board.transform, false);
		board.transform.localScale = scale;
		for (int y = 0; y < size; y++) {
			for (int x = 0; x < size; x++) {
				GameObject cell = (GameObject)Instantiate (cellPrefab);  
				cell.name = "Cell " + x + "-" + y;
				cell.transform.SetParent (cellsParent.transform);
				cell.transform.localPosition = new Vector2 (x, y);
				cell.transform.localScale = new Vector2 (1, 1);
				CellData cellData = cell.AddComponent <CellData> ();
				cellData.x = x;
				cellData.y = y;
				cellData.board = boardData;
				boardData.cells [x, y] = cellData;

//				SpriteRenderer cellSprite = cell.AddComponent<SpriteRenderer> ();
//				cellSprite.sprite = cellImage;
//				EventTrigger cellEvent = cell.AddComponent<EventTrigger> ();
//				EventTrigger.Entry entry = new EventTrigger.Entry();
//				entry.eventID = EventTriggerType.PointerClick;
//				entry.callback = new EventTrigger.TriggerEvent();
//				UnityAction<BaseEventData> l_callback = new UnityAction<BaseEventData>(Teste);
//				entry.callback.AddListener( l_callback );
//				cellEvent.triggers.Add(entry);
			}
		}
		cellsParent.transform.localRotation = Quaternion.Euler(new Vector3 (45, 0, -45));
		return boardData;
	}

//	public void Teste(UnityEngine.EventSystems.BaseEventData baseEvent){
//		Debug.Log (this.name);
//	}
}
